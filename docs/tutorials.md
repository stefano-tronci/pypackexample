## Using the Path to Data

Use [`mod1.get_data_path`][pypackexample.mod1.get_data_path] to load the data path:

```python
from src.pypackexample import mod1

path = mod1.get_data_path()
```

Now you can use it as you wish. For example, if it contained a numpy array:

```python
import numpy as np

data = np.genfromtxt(path, delimiter=',')
```

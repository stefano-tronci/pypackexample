# Welcome to pyPackExample

Official Documentation for the [pyPackExample](https://gitlab.com/stefano-tronci/pypackexample) project.

`pyPackExample` is a minimal Python package example. It includes unit tests and documentation. A step by step guide in package creation is provided in these tutorials:

* [Hosting Python Packages on Git Repositories](https://stefano-tronci.gitlab.io/website/posts/hosting-python-packages-on-git-repositories/).
* [Hosting Python Packages on Git Repositories - Update](https://stefano-tronci.gitlab.io/website/posts/hosting-python-packages-on-git-repositories-2/).
* [Hosting Python Packages on Git Repositories - Another Update](https://stefano-tronci.gitlab.io/website/posts/hosting-python-packages-on-git-repositories-3/).

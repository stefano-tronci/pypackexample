## How to Call `function3` From `mod2`?

Just do:

```python
from src.pypackexample import mod2

success = mod2.function3(data=[1.0, 2.0, 3.0])
```

## How to Run the Unit Tests?

The tests are not distributed with the package. In order to run the tests you must clone the repository:

```commandline
git clone git@gitlab.com:stefano-tronci/pypackexample.git
```

The `cd` into the repository, install from the local copy and run the tests. If you wish to use a virtual environment, activate it now.
```commandline
# Activate your virtual environment now if you wish to install pyPackexample to it.
cd pypackexample
pip install .
python -m unittest discover 
```

## I See a `shell.nix` File in There! :eyes:

The `shell.nix` file in the root of the repository will create a nix shell with most of the dependencies of this package.
Then, it will create a virtual environment in `.venv`, and it will activate it.

If you lan to develop on a Nix system use (from the root of the repo):

```commandline
nix-shell
```

And then:

```commandline
pip install -e .[docs]
```

The virtual environment should carry on working as a normal virtual environment, even in your IDE of choice (tested on pyCharm).

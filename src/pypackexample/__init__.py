"""A Python Package Example. Includes unit tests and documentation.

Modules exported by this package:

- `main`: Basic main example for entry point.
- `mod1`: Basic module example.
- `mod2`: Basic module example which imports `mod1`.

"""
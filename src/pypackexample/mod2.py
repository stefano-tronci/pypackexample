"""
`mod2` Module
===================

A simple module example importing another module.
"""

from src.pypackexample import mod1


def function2(*args, **kwargs) -> None:
    """
    Simple example function. Takes any arguments, does nothing. This wraps ``mod1.function1``.

    ```python title="Example"
    from pypackexample import mod2
    mod2.function2(1, 2, 0.4, 'dog', cat='sleepy')
    ```

    :param args: Anything you want.
    :param kwargs: Anything you want.
    :return: Nothing.

    """
    mod1.function1(*args, **kwargs)


def function3(data: list[float]) -> bool:
    r"""
    This function just illustrates the power of type hints with `mkdocstring`.

    It does actually nothing, but the type hints will appear in the docs. But we can pretend it is doing some maths:

    $$
    \operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
    $$

    (equation taken from [here](https://squidfunk.github.io/mkdocs-material/reference/math/#docsjavascriptsmathjaxjs)).

    ```python title="Example"
    from pypackexample import mod2
    success = mod2.function3(data=[1.0, 4.5, 4.5])
    ```

    :param data: A list that mocks some data for processing.
    :return: `True` if processing was successful
    """
    return True

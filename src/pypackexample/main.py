"""
`main` Module
===================

The `main` module provides a single `main` function that is called when the file is
directly executed.  This module serves as an entry point for the package.
"""

from src.pypackexample import mod1


def main() -> bool:
    """
    The main function. This is executed from the `pypackexample_main` entry point.
    This function will display a path to package data and return `True` if the path exists.

    Run this into the console (activate your virtual environment first if any) to run:

    ```commandline title="Example"
    pypackexample_main
    ```

    :return: `True` if the data path exists.
    """
    path = mod1.get_data_path()
    print(f'Data Path is {path.absolute().as_posix():s}')
    return path.exists()


if __name__ == '__main__':
    if main():
        print('The path exists!')
    else:
        print('The path does not exists!')

"""
`mod1` Module
===================

A simple module example.
"""

from pathlib import Path


def function1(*args, **kwargs) -> None:
    """
    Simple example function. Takes any arguments, does nothing.

    ```python title="Example"
    from pypackexample import mod1
    mod1.function1(1, 2, 0.4, 'dog', cat='sleepy')
    ```

    :param args: Anything you want.
    :param kwargs: Anything you want.
    :return: Nothing.

    """
    pass


def get_data_path() -> Path:
    """
    Get the path to the bundled ``data.csv`` file, as a ``pathlib.Path`` object.

    ```python title="Example"
    from pypackexample import mod1
    data_path = mod1.get_data_path()
    ```

    :return: path to the bundled ``data.csv`` file.

    """
    return Path(__file__).parent.joinpath('data', 'data.csv')

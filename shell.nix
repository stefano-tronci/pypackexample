{ pkgs ? import <nixpkgs> {} }:

let
  pythonEnv = pkgs.python3.withPackages (ps: with ps; [
    pip
    virtualenvwrapper
    setuptools
    wheel
    mkdocs
    mkdocstrings-python
    mkdocs-material
  ]);
in
pkgs.mkShell {
  buildInputs = [ pythonEnv ];
  shellHook = ''
    export VIRTUAL_ENV=$(pwd)/.venv
    python -m venv $VIRTUAL_ENV
    source $VIRTUAL_ENV/bin/activate
  '';
}

# pyPackExample

A Python Package Example. Includes unit tests and documentation. This repository is an example for these tutorials:

* [Hosting Python Packages on Git Repositories](https://stefano-tronci.gitlab.io/website/posts/hosting-python-packages-on-git-repositories/).
* [Hosting Python Packages on Git Repositories - Update](https://stefano-tronci.gitlab.io/website/posts/hosting-python-packages-on-git-repositories-2/).
* [Hosting Python Packages on Git Repositories - Another Update](https://stefano-tronci.gitlab.io/website/posts/hosting-python-packages-on-git-repositories-3/).

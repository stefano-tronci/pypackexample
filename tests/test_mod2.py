import unittest
from src.pypackexample import mod2


class TestMod2(unittest.TestCase):

    def test_function2(self) -> None:
        self.assertIsNone(mod2.function2())

    def test_function3(self) -> None:
        self.assertTrue(mod2.function3(data=[1.0, 2.0, 3.0]))


if __name__ == '__main__':
    unittest.main()

import unittest
from src.pypackexample import mod1


class TestMod1(unittest.TestCase):

    def test_function1(self) -> None:
        self.assertIsNone(mod1.function1())

    def test_get_data_path(self) -> None:
        self.assertTrue(mod1.get_data_path().exists())


if __name__ == '__main__':
    unittest.main()

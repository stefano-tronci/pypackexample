import unittest
from src.pypackexample import main


class TestMain(unittest.TestCase):

    def test_main(self) -> None:
        self.assertTrue(main.main())


if __name__ == '__main__':
    unittest.main()
